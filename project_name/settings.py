"""
Django settings for {{ project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/{{ docs_version }}/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf import settings
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/{{ docs_version }}/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '{{ secret_key }}'

# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG COLLECTIONS ;)
# True for testing
# False for production
import socket
# If this app is stored at your server, then DEBUG is False
HOSTNAME = '<YOUR_HOST_NAME>'
DEBUG = socket.gethostname() != HOSTNAME
# DEBUG = True

TEMPLATE_DEBUG = DEBUG # If False, show error messages when error occurs

STATIC_DEBUG = DEBUG # If False, use minimized JS/CSS

DATABASE_DEBUG = DEBUG # If False, should use more 'powerful' database

TASTYPIE_FULL_DEBUG = DEBUG # IF False, return error when error occurs

MAINTENANCE_MODE = False # for production, give False


ALLOWED_HOSTS = ['*']
# INTERNAL_IPS = ()


# Application definition

INSTALLED_APPS = (
    '{{ project_name }}',
    'tinycms',
    'tinycms.facebook',
    'tastypie',
    'mptt',
    'taggit',
    'polymorphic',
    'registration',
    'sorl.thumbnail',
    'maintenancemode',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.sites',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    # 'debug_toolbar',
    'gunicorn'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'maintenancemode.middleware.MaintenanceModeMiddleware'
)

ROOT_URLCONF = '{{ project_name }}.urls'

WSGI_APPLICATION = '{{ project_name }}.wsgi.application'


# Database
# https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/#databases

if DATABASE_DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
else:
    # use more sophisticated sql e.g. MySQL, PostgreSQL, Oracle, etc.
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': '<YOUR_DATABASE_NAME>',
            'USER': '<YOUR_DATABASE_USERNAME>',
            'PASSWORD': '<YOUR DATABASE_PASSWORD>',
            'HOST': '<YOUR_DATABASE_HOST>', # database host
            'PORT': '3306' # your database port, default is 3306
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/{{ docs_version }}/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Jakarta'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

APPEND_SLASH = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/{{ docs_version }}/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'project_static_files'),
)

STATICFILES_FINDERS = (
    'tinycms.finders.StaticFilesFinder',
) + settings.STATICFILES_FINDERS

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'project_templates'),
)

TEMPLATE_LOADERS = (
    'tinycms.finders.Loader',
) + settings.TEMPLATE_LOADERS

LOGIN_REDIRECT_URL = '/'

TEMPLATE_CONTEXT_PROCESSORS = settings.TEMPLATE_CONTEXT_PROCESSORS + (
   'django.core.context_processors.request',
   'tinycms.context_processors.settings'
)

MAINTENANCE_IGNORE_URLS = (
    r'^/admin',
    r'^/accounts',
    r'^/manage',
)

AUTHENTICATION_BACKENDS = settings.AUTHENTICATION_BACKENDS + (
    'tinycms.auth_backends.EmailBackend',
    'tinycms.facebook.auth_backends.FacebookBackend'
)


SERVER_EMAIL = 'dev@example.com'
DEFAULT_FROM_EMAIL = 'info@example.com'

ADMINS = (
    ('Aditya Darmawan', 'aditya@kus-tom.com'),
)

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'hello@kus-tom.com'
EMAIL_HOST_PASSWORD = '21uelxRG1IbYzchM4ZGYLw'
EMAIL_USE_TLS = True

CMS_THEME = '{{ project_name }}'

AUTH_USER_MODEL = '{{ project_name }}.User'
# CMS_MODEL_CATEGORY = '{{ project_name }}.Category'
# CMS_MODEL_IMAGE = '{{ project_name }}.Image'

CMS_API_USER = '{{ project_name }}.resources.UserResource'
# CMS_API_CATEGORY = '{{ project_name }}.resources.CategoryResource'
# CMS_API_IMAGE = '{{ project_name }}.resources.ImageResource'

CONFIG_ACCESSIBLE = []

REGISTRATION_OPEN = True # False if registration is not allowed
ACCOUNT_ACTIVATION_DAYS = 0
SEND_ACTIVATION_EMAIL = False

THUMBNAIL_PREFIX = 'thumbnail/'
THUMBNAIL_BACKEND = 'tinycms.thumbnail.KeepNameThumbnailBackend'
THUMBNAIL_SIZE = {
    'SMALL': 120,
    'MEDIUM': 300,
    'LARGE': 480
}

# RECAPTCHA_SECRET_KEY = '6LdEVQUTAAAAAA-qJVhraCEP64qBOSPXyR5tqNRW'

try:
    from local_settings import *
except ImportError:
    pass