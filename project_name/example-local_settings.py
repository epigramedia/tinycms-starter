DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '<YOUR-DATABASE-NAME>', # database name, ideally equals to project name
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '/opt/lampp/var/mysql/mysql.sock', # database host
    }
}