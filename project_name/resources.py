from tinycms.resources import (
    BaseUserResource,
    # BaseCategoryResource,
    # BaseImageResource
)

from .models import *


class UserResource(BaseUserResource):
    class Meta(BaseUserResource.Meta):
        pass


# class CategoryResource(BaseCategoryResource):
#     class Meta(BaseCategoryResource.Meta):
#         pass


# class ImageResource(BaseImageResource):
#     class Meta(BaseImageResource.Meta):
#         pass


api_list = (
    UserResource(),
    # CategoryResource(),
    # ImageResource()
)