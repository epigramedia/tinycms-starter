from django.conf.urls import patterns, include, url
from django.contrib import admin

from tastypie.api import Api
from tinycms.resources import api_list as tinycms_api_list
from .resources import api_list as {{ project_name }}_api_list

from .views import HomeView

v1_api = Api(api_name='v1')
v1_api.register_multiple(tinycms_api_list)
v1_api.register_multiple({{ project_name }}_api_list)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', '{{ project_name }}.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'', include('tinycms.urls'))
)