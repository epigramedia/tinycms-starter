var HomeController = ['$scope', '$timeout', 'tastypieService', function($scope, $timeout, tastypieService) {
    var patientService = new tastypieService('/api/v1/patient/');

    $scope.getList = function(is_first) {
        // modify meta
        var meta = {};
        angular.copy($scope.meta, meta);

        _.each(meta, function(el, key) {
            if (!el) {
                delete meta[key];
            }
        });

        // get list
        patientService.getList(meta).then(
            function(results) {
                if (is_first) {
                    delete $scope.object_list;
                    $scope.object_list = [];
                    angular.copy(results.data, $scope.object_list);
                } else {
                    $scope.object_list = $scope.object_list.concat(results.data);
                }

                $scope.meta_object = {};
                angular.copy(results.meta, $scope.meta_object);
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );
    };

    $scope.goFirst = function() {
        $scope.object_list = [];
        $scope.meta.page = 1;
        $scope.getList(true);
    };

    var _timeout;
    $scope.onSearch = function() {
        $timeout.cancel(_timeout);

        $timeout(function() {
            $scope.goFirst();
        }, 2000);
    };

    $scope.loadMore = function() {
        $scope.meta.page += 1;
        $scope.getList();
    };

    $scope.init = function() {
        console.log('Entering Home Controller');
        patientService.clearCache();
        // init meta
        $scope.meta = {
            fuzzy: '',
            page: 1,
            limit: 50,
            _fields: 'id,name,dob,parent_1,parent_2,resource_uri'
        };
        $scope.goFirst();
    };

    $scope.init();
}];