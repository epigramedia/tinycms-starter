// Requires underscore.js

var getIdFromResourceUri = function(resource_uri) {
    var temp = /\/(\d+)\//g.exec(resource_uri);

    return temp[1];
};

angular.module('ng.tastypie', [])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.cache = true;
}])

.factory('tastypieService', ['$http', '$q', '$cacheFactory', function($http, $q, $cacheFactory) {
    var _tastypieService = function(param) {
        var that = this;
        this.param = param;
        this.object = {};
        this.object_list = [];
        this.original_data = {};
        this.meta = {};

        // PRIVATE HELPER METHODS
        // add trailing slash to url
        this._urlTrailingSlash = function(url) {
            if (url[url.length-1] != '/') {
                url += '/';
            }

            return url;
        };

        // Converts object to url param string
        this._objToParamStr = function(obj) {
            if (_.isEmpty(obj)) return '';

            if (angular.isObject(obj)) {
                // if object
                var temp = [];

                _.each(obj, function(el, key) {
                    var this_el;
                    if (angular.isArray(el)) {
                        this_el = el.join(',');
                    } else {
                        this_el = el.toString();
                    }
                    this.push(key + '=' + this_el);

                }, temp);

                results = '?' + temp.join('&');

            } else if (angular.isString(obj) && obj[0] != '?') {
                // if string
                results = '?' + obj;
            } else {
                results = obj;
            }

            return results;
        };

        // Build uri from specified id
        this._buildUri = function(id) {
            return that._urlTrailingSlash(that.url) + id + '/';
        };

        this._unfull_field = function(obj) {
            return obj.resource_uri || obj;
        };

        this._unfull_field_list = function(obj_list) {
            return _.compact(_.map(obj_list, _unfull_field));
        };

        this._unfull_row = function(row, fields) {
            var tempObject = {};
            angular.copy(row, tempObject);

            // get fields
            if (!fields) {
                fields = _.keys(fields);
            } else if (!angular.isArray(fields)) {
                fields = [fields];
            }

            _.each(fields, function(el, idx) {
                if (!angular.isArray(this[el])) {
                    // fk
                    this[el] = that._unfull_field(this[el]);
                } else {
                    // m2m
                    this[el] = that._unfull_field_list(this[el]);
                }
            }, tempObject);

            return tempObject;
        };
        // END OF PRIVATE HELPER METHODS

        // UNFULL
        this.unfull = function(object, fields) {
            return that._unfull_row(object, fields);
        };

        this.unfullList = function(object_list, fields) {
            return _.map(object_list, function(x) {
                return _.unfull_row(x, fields);
            });
        };
        // END OF UNFULL

        // GETTERS
        this.getObject = function() {
            return that.object;
        };

        this.getObjectList = function() {
            return that.object_list;
        };

        this.getMeta = function() {
            return that.meta;
        };

        this.getOriginalData = function() {
            return that.original_data;
        };

        this.getIdFromResourceUri = function(resource_uri) {
            var temp;
            resource_uri = that._urlTrailingSlash(resource_uri);
            temp = /\/(\d+)\//g.exec(resource_uri);

            return temp[1];
        };

        this.setUrl = function(url) {
            that.url = that._urlTrailingSlash(url);
        };

        // fallback
        this.getObjectFromServer = this.getObject;
        this.getObjectListFromServer = this.getObjectList;
        this.getMetaFromServer = this.getMeta;

        // END OF GETTERS

        // APIS
        // get an object via id or detail uri or resource uri (GET method)
        // the result is available via results.data
        this.get = function(id, params) {
            var deferred = $q.defer(),
                uri;

            // build uri
            if (!that.url || (angular.isString(id) && id.indexOf(that.url) == 0)) {
                // if id is resource_uri
                uri = id;
            } else if (that.url) {
                uri = that._buildUri(id);
            }

            uri += that._objToParamStr(params);

            $http.get(uri).then(
                function(results) {
                    var the_results = {};
                    angular.copy(results, the_results);

                    angular.copy(results.data, that.object);
                    angular.copy(results.data, that.original_data);

                    the_results.data = {};

                    angular.copy(that.object, the_results.data);

                    deferred.resolve(the_results);
                },
                function(errors) {
                    deferred.reject(errors);
                },
                function(value) {
                    deferred.notify(value);
                }
            );

            return deferred.promise;
        };

        // get data list (GET method) (filter & order capable [if specified])
        // ATTENTION: The result is available via results.data (NOT results.data.objects)
        //            meta can be found at results.meta
        this.getList = function(params) {
            var deferred = $q.defer(),
                url = that.url + that._objToParamStr(params);

            $http.get(url).then(
                function(results) {
                    var the_results = {};
                    angular.copy(results, the_results);

                    angular.copy(results.data.meta, that.meta);
                    angular.copy(results.data.objects, that.object_list);
                    angular.copy(results.data, that.original_data);

                    the_results.meta = {};
                    the_results.data = [];

                    angular.copy(that.meta, the_results.meta);
                    angular.copy(that.object_list, the_results.data);

                    deferred.resolve(the_results);
                },
                function(errors) {
                    deferred.reject(errors);
                },
                function(value) {
                    deferred.notify(value);
                }
            );

            return deferred.promise;
        };

        // update (put) if obj has resource_uri, otherwise create (post)
        this.save = function(obj, params) {
            var method, url;
            if (_.has(obj, 'resource_uri')) {
                method = 'PUT';
                url = obj.resource_uri;
            } else {
                method = 'POST';
                url = that.url;
            }

            url += that._objToParamStr(params);

            return $http({
                method: method,
                url: url,
                data: obj
            });
        };

        // delete an object by id, detail uri or resource uri
        this.delete = function(id, params) {
            var uri;

            // build uri
            if (!that.url || (that.url && angular.isString(id) && id.indexOf(that.url) == 0)) {
                // if id is resource_uri
                uri = id;
            } else {
                uri = that._buildUri(id);
            }

            uri += that._objToParamStr(params);

            return $http({
                method : 'DELETE',
                url : uri
            });
        };

        // the bulk operation (creating, updating, and deleting) (PATCH method)
        // First param is objects to create or update
        // Second param is objects to delete
        // Third param is ONLY objects to create
        // Forth param is self-explained ;)

        this.saveAll = function(objects, deleted_objects, new_objects, params) {
            if (!deleted_objects) {
                deleted_objects = []
            }

            if (!new_objects) {
                new_objects = [];
            }

            var saved_objects = {
                objects: _.union(objects, new_objects),
                deleted_objects: deleted_objects
            };

            var url = that.url + that._objToParamStr(params)

            return $http({
                method : 'POST',
                url : url,
                data : saved_objects,
                headers : {
                    'X-HTTP-Method-Override' : 'PATCH'
                }
            });
        };
        // END OF APIS

        // clears the cache
        // cache is true by default to prevent accessing server over and over again.
        this.clearCache = function() {
            var cache = $cacheFactory.get('$http');
            cache.removeAll();
        };

        // for non-standard urls
        this.api = function(http_obj_param) {
            http_object_parameter = {
                method : 'GET',
                url: that.url
            };

            // angular.extend(thisClass.options, thisClass.optionsParam);
            if (http_obj_param) {
                angular.extend(http_object_parameter, http_obj_param);
            }

            return $http(http_object_parameter);
        };

        this.init = function() {
            // get resource url
            // init is no longer receiving other options except api url
            var url;
            if (angular.isObject(param)) {
                // fallback
                url = param.apiUrl;
            } else {
                url = param;
            }

            that.setUrl(url);
        };

        // INITIALIZATION
        this.init();
    };

    return _tastypieService;
}]);